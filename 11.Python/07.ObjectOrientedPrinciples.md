### Goals
- The trainee will understand the concepts of cohesion and coupling, and know to aim for high cohesion and low coupling
- The trainee will understand what interfaces are and why they are useful
- The trainee will understand what dependency injection is and how it is used to reduce coupling
- The trainee will learn about and understand the SOLID principles
- The trainee will understand the concept of design patterns and will be familiar with few common examples
### Tasks

### Notes

### References

# Python Development

### Introduction
- Python Introduction

### Procedural Programming
- Clean code
    - Meaningful Names
    - Efficient Commenting
    - Division to Functions
    - KISS & DRY Principles
- Modules
- Exception Handling


### Typing
- Python Typing System
- Type Hinting
- Common Types
- `typing`
- `mypy`
- Generics

### Object Oriented Programming
- Classes
    - Getters and Setters
    - Inheritence
    - Law of Demeter
    - Abstract Classes
    - Dunder Methods
    - `dataclass`
- OO Principles
    - Coupling and Cohesion
    - Interfaces
    - Polymorphism
    - Dependency Injection
    - SOLID
    - Design Patterns

### Python Specific Tools (Extra)
- List Comprehension
- Packing (`*`, `**`)
- Context Managers (`with`)
- Functional Programming
    - Anonymous Functions (`lambda`)
    - `map`, `filter`, `reduce`
    - Composition
    - Pure Functions
    - Side Effects
    - High Order Functions
    - Decorators
- Iterators
- Useful Modules
    - `collections`
    - `functools`
    - `itertools`
- Generators
- Parallelism And Concurrency
    - Thread VS Process
    - Python GIL
    - Parallelism VS Concurrency (CPU VS I/O)
    - `threading`
    - `multiprocessing`
    - `asyncio`
